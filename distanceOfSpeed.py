# distanceOfSpeed.py: this script determines the distance to a lightning strike based on
# the time elapsed between the flash and sound of thunder 

# The constraints 
SPEED_OF_SOUND = 1100    #ft / sec
MILE_IN_FEET = 5200 # ft

# obtain the time elapsed from the user
time = int(input("Enter the time elapsed between the flash and the " + \
    "sound of thunder in seconds: "))   #sec

# Calculations
distance = SPEED_OF_SOUND * time   #ft
miles = distance // MILE_IN_FEET
feet = distance % MILE_IN_FEET

#Display the result for the user
print("\nThe distance to the lightning strike is about", miles, "miles and", \
    feet, "feet from your current location.")
print("\nThank you.")