# twoPointsinAPlane.py: scripts which calculates the slope from the cordinates of two points
# Author: Stephen Isiuwe
# Date: 9/6/2021

def main():
    # get the cordinates from the user
    x1 = int(input("What is x1? "))
    x2 = int(input("What is x2? ")) 
    y1 = int(input("What is y1? "))
    y2 = int(input("What is y2? "))
    
    # clculate slope
    slope = float((y2 - y1) / (x2 - x1))
    
    # print the result for the user
    print("\nThe slope of the line is", round(slope, 2))
    print("\nThank you")
main()

