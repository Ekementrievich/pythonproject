# sphereVolumeArea.py: Script used to calculae the volume and area
# of a sphere
# Author: Stephen Isiuwe
# Date: 9/6/2021

from math import pi

def main():
    radius = eval(input("Enter the radius of the sphere: "))

    # Calculate the volume of the sphere based on the formular
    volume = (4/3) * pi * (radius ** 3)

    # Calculate the surface area of the sphere based on the formular
    surfArea = 4 * pi * (radius **2)

    #print the results
    print ("\nThe volume of the sphere is %.2f." % volume)
    print ("\nThe surface area of the sphere is %.2f." % surfArea)
    print ("\nThanks")
main()