# Requires pymongo 3.6.0+
from bson.son import SON
from pymongo import MongoClient

client = MongoClient('mongodb+srv://Stephen:Adejumo1801!!!!!!!!!!!!!!!@cluster0.8c8vj.mongodb.net/test')
database = client["demo-sql"]
collection = database["housing"]

# Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

pipeline = [
    {
        u"$group": {
            u"_id": {
                u"zip_code": u"$zip_code"
            },
            u"COUNT(*)": {
                u"$sum": 1
            },
            u"MAX(units)": {
                u"$max": u"$units"
            }
        }
    }, 
    {
        u"$project": {
            u"_id": 0,
            u"COUNT(*)": u"$COUNT(*)",
            u"MAX(units)": u"$MAX(units)",
            u"zip_code": u"$_id.zip_code"
        }
    }, 
    {
        u"$sort": SON([ (u"MAX(units)", -1) ])
    }, 
    {
        u"$project": {
            u"_id": 0,
            u"COUNT(*)": u"$COUNT(*)",
            u"MAX(units)": u"$MAX(units)",
            u"zip_code": u"$zip_code"
        }
    }
]

cursor = collection.aggregate(
    pipeline, 
    allowDiskUse = False
)
try:
    for doc in cursor:
        print(doc)
finally:
    client.close()