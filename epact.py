#epact.py: this is a script to calculate the 
# the number of days between January 1st and the previous 
# new moon

year = int(input("Enter the year: "))

# calculate C
C = year // 100

# calculate epact
epact = (8 + (C//4) - C + (((8 * C) +13)//25) + 11*(year%19))%30

#print the output
print("The epact is ",epact)