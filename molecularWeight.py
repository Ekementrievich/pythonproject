# molecularWeight.py: Script to calculate the molecular weight of a 
# hydrocarbon given the individual weights of the components

H_WEIGHT = 1.0079   # grams/mole
C_WEIGHT = 12.011   # grams/mole
O_WEIGHT = 15.994   # grams/mole

# obtain the number of each element in the molecule from the user 
hydrogen = int(input("Enter the number of H atoms in the hydrocarbon: "))
carbon = int(input("Enter the number of C atoms in the hydrocarbon: "))
oxygen = int(input("Enter the number of O atoms in the hydrocarbon: "))

#calculations
total_H_weight = hydrogen * H_WEIGHT
total_C_weight = carbon * C_WEIGHT
total_O_weight = oxygen * O_WEIGHT
total_weight = total_H_weight + total_C_weight + total_O_weight

# Display the result for the user
print("The moleculr weight of the hydrocarbon is", round(total_weight, 2), \
    "grams per mole")
print("\nThank you ")