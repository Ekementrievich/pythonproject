# theKonditoreicoffeeShop.py: Scriptt to calculate the cost of an order given various variables

# the constraints
COST_PER_POUND = 10.5
SHIPPING_PER_POUND = 0.86
SHIPPING_OVERHEAD = 1.5

# Obtain the number of pounds from the user
pounds = int(input("Enter the number of pounds you wish to purchase: "))

#calculations
subtotal = pounds * COST_PER_POUND   #pre-shipping cost
shipping = SHIPPING_OVERHEAD + pounds + SHIPPING_PER_POUND
total = subtotal + shipping

# Display the result for the user
print("\nThe order will cost $",round(total, 2), " in total.")
print("\nThank you.")