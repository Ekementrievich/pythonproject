# costPerSquareInchOfPizza.py: Script to calculate the cost per square inch of a pizza
# given the diameter and price. The formula for the area is A = piR**2

from math import cos, pi

def main():
    # Obtain the diameter and price from the user
    diameter = eval(input("Enter the diameter of the pizza in inches: "))
    cost = float(input("Enter the cost of the pizza: "))

    #calculate the radius, area and per square inch 
    radius  = diameter / 2

    area = pi * (radius ** 2)

    cost_per_inch = cost / area

    # print the result
    print ("The cost per square inch of the pizza is $", round(cost_per_inch, 2), " per square inch")


main()