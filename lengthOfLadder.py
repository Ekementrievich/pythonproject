# lengthOfLadder.py: Script to calculate the length of the 
# when the height and angle is given by the user

from math import pi, sin

def main():
    # get the height of the ladder and the angle
    height = float(input("What is the height of the ladder? "))
    angle = float(input("What is the angle of the ladder? "))
    
    # calculate the length of the ladder
    angle_in_rad = angle * pi / 180
    length = height / sin(angle_in_rad)
    
    #give the result to the user
    print("The length of the ladder is: ",round(length, 2))
main()