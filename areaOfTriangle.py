# areOfTriangle.py: Script to calculate the area of a triangle given the the
# length of its three sides

from math import sqrt

def main():
    # ask user for the value of each side
    a = int(input("What is a? "))
    b = int(input("What is b? "))
    c = int(input("What is c? "))
    
    # calculate S
    s = (a + b + c) / 2
    
    #calculate Area
    A = float(sqrt(s * (s - a) * (s - b) * (s - c)))
    
    # print the result for the user
    print("\nThe area of the triangle is ",round(A, 2))
    print("\nThank you.")
main()