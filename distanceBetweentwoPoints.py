# distanceBetweenTwoPoints.py: Script to calculate the distance between two points 
# Author: Stephen Isiuwe
# 9/6/2021

from math import sqrt

def main():
    # Get the coordinates of the two points
    x1 = int(input("What is x1? "))
    x2 = int(input("What is x2? "))
    y1 = int(input("What is y1? "))
    y2 = int(input("What is y2? "))
    
    # Calculate the distance
    distance = float(sqrt(((x2 - x1)**2) + ((y2 - y1)**2) ))
    
    #print the result for the user
    print("The distance between the two points is ",round(distance, 2))
main()
